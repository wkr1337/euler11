/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eulerproblem11;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Willem
 */
public class EulerProblem11 {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        EulerProblem11 mainInstance = new EulerProblem11();
        mainInstance.start();
    }

    private void start() throws IOException {
        String fileName = "C:\\Users\\Willem\\Documents\\NetBeansProjects\\gff3\\EulerProblem11\\data\\20x20Grid.txt";
        readFile(fileName);
    }

    private ArrayList readFile(String givenFile) throws FileNotFoundException, IOException {
        ArrayList myList = new ArrayList<>();
        ArrayList<String> fileBuiler = new ArrayList<String>();
        int largestProduct = 0;
        BufferedReader bufferdReaderInstance = new BufferedReader(new FileReader(givenFile));

        String line = bufferdReaderInstance.readLine();

        int lineCounter = 0;
        while (line != null) {

            fileBuiler.add(line + "\n");
            line = bufferdReaderInstance.readLine();

        }

        for (String fileBuilerLine : fileBuiler) {

            String[] splittedLine = fileBuilerLine.split(" ");
            int positionCounter = 0;
            for (String splittedLine1 : splittedLine) {

                if (lineCounter < 17) {
                    int sumOfNumbersDown = Integer.parseInt(fileBuiler.get(0).split(" ")[positionCounter].trim()) * Integer.parseInt(fileBuiler.get(1).split(" ")[positionCounter].trim()) * Integer.parseInt(fileBuiler.get(2).split(" ")[positionCounter].trim()) * Integer.parseInt(fileBuiler.get(3).split(" ")[positionCounter].trim());
                    if (sumOfNumbersDown > largestProduct) {
                        System.out.println("sumOfNumbersDown = " + sumOfNumbersDown);
                        largestProduct = sumOfNumbersDown;
                    }
                }
                if (positionCounter < 17) {
                    int sumOfNumbersRight = Integer.parseInt(splittedLine[positionCounter].trim()) * Integer.parseInt(splittedLine[positionCounter + 1].trim()) * Integer.parseInt(splittedLine[positionCounter + 2].trim()) * Integer.parseInt(splittedLine[positionCounter + 3].trim());
                    if (sumOfNumbersRight > largestProduct) {
                        System.out.println("sumOfNumbersRight = " + sumOfNumbersRight);
                        largestProduct = sumOfNumbersRight;
                        System.out.println("positionCounter " + positionCounter + " lineCounter = " + lineCounter);
                    }
                }
                if (lineCounter < 17 && positionCounter < 17) {
                    int sumOfNumbersDiag = Integer.parseInt(fileBuiler.get(lineCounter).split(" ")[positionCounter].trim()) * Integer.parseInt(fileBuiler.get(lineCounter + 1).split(" ")[positionCounter + 1].trim()) * Integer.parseInt(fileBuiler.get(lineCounter + 2).split(" ")[positionCounter + 2].trim()) * Integer.parseInt(fileBuiler.get(lineCounter + 3).split(" ")[positionCounter + 3].trim());
                    if (sumOfNumbersDiag > largestProduct) {
                        System.out.println("sumOfNumbersDiag = " + sumOfNumbersDiag);
                        largestProduct = sumOfNumbersDiag;
                    }
                }
                if (lineCounter < 17 && positionCounter > 3) {

                    int sumOfNumbersDiag2 = Integer.parseInt(fileBuiler.get(lineCounter).split(" ")[positionCounter].trim()) * Integer.parseInt(fileBuiler.get(lineCounter + 1).split(" ")[positionCounter - 1].trim()) * Integer.parseInt(fileBuiler.get(lineCounter + 2).split(" ")[positionCounter - 2].trim()) * Integer.parseInt(fileBuiler.get(lineCounter + 3).split(" ")[positionCounter - 3].trim());
                    if (sumOfNumbersDiag2 > largestProduct) {
                        largestProduct = sumOfNumbersDiag2;
                        System.out.println("sumOfNumbersDiag2 = " + lineCounter);
                        System.out.println("sumOfNumbersDiag2 = " + positionCounter);
                    }
                }

                positionCounter++;
            }
            lineCounter++;
        }
        System.out.println("largestProduct = " + largestProduct);
        return myList;

    }
}
